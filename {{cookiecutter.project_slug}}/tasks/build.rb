namespace :build do
  desc 'build all gems'
  task all: 'test:all' do
    PLUGINS.each do |plugin|
      plugindir = "#{PLUGIN_ROOT}/#{plugin}"
      gemfile = "#{plugindir}/Gemfile"

      Dir.chdir(plugindir) do
        Bundler.with_clean_env do
          ENV['BUNDLE_GEMFILE'] = gemfile
          system('bundle exec rake build')
        end
      end
    end
  end
end
