namespace :release do
  desc 'release all gems'
  task all: 'build:all' do
    PLUGINS.each do |plugin|
      gem_name = "#{plugin}-#{VERSION}.gem"
      plugin_path = "#{PLUGIN_ROOT}/#{plugin}"
      artifact_path = "#{plugin_path}/target/artifacts/#{plugin}/#{VERSION}"
      gem_path = "#{artifact_path}/#{gem_name}"

      system("gem push #{gem_path}")
    end
  end
end

