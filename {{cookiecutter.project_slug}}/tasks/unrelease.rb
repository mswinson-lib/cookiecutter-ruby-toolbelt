namespace :unrelease do
  desc 'unrelease all gems'
  task all: 'setup:all' do
    PLUGINS.each do |plugin|
      system("gem yank #{plugin} -v #{VERSION}")
    end
  end
end
