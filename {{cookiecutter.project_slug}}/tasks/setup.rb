namespace :setup do
  file "#{TARGETDIR}/.setup" do
    PLUGINS.each do |plugin|
      plugin_dir = "#{PLUGIN_ROOT}/#{plugin}"
      gemfile = "#{plugin_dir}/Gemfile"

      Bundler.with_clean_env do
        Dir.chdir(plugin_dir) do
          ENV['BUNDLE_GEMFILE'] = gemfile
          system("bundle install")
        end
      end
    end
    FileUtils.mkdir_p(TARGETDIR)
    FileUtils.touch("#{TARGETDIR}/.setup")
  end

  desc 'setup all gems'
  task all: "#{TARGETDIR}/.setup"
end
