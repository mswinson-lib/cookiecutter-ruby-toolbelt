namespace :test do
  desc 'run all tests'
  task all: 'setup:all' do
    PLUGINS.each do |plugin|
      plugindir = "#{PLUGIN_ROOT}/#{plugin}"
      gemfile = "#{plugindir}/Gemfile"

      Dir.chdir(plugindir) do
        Bundler.with_clean_env do
          ENV['BUNDLE_GEMFILE'] = gemfile
          system('bundle exec rake test:all')
        end
      end
    end
  end
end
