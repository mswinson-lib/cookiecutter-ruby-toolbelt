# {{cookiecutter.project_name}}

## Installation

    gem install {{cookiecutter.project_slug}}

## Usage

setup

    bundle install
    rake setup:all

run tests

    rake test:all

release all

    rake release:all



## Contributing

1. Fork it ( <%= "{{cookiecutter.project_home}}/fork" %> )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new pull request
