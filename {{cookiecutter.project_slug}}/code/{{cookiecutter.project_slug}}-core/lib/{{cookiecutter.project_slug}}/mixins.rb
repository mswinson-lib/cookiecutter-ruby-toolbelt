require '{{cookiecutter.project_slug}}/mixins/registered'
require '{{cookiecutter.project_slug}}/mixins/pluggable'
require '{{cookiecutter.project_slug}}/mixins/initializable'
require '{{cookiecutter.project_slug}}/mixins/configurable'
