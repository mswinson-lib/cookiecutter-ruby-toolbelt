module {{cookiecutter.project_klass}}
  # object registry
  class Registry
    # registry key
    class Key
      attr_accessor :key
      attr_accessor :value

      private

      # initialize key
      # @api private
      # @param key [String] registry key
      # @param value [Object] registry value
      def initialize key, value
        @key = key
        @value = value
      end
    end

    class << self
      # register key
      # @api public
      # @param key [String] key
      # @param value [Object] key value
      # @return void
      def register key, value
        if store[key]
          store[key].value = value
        else
          store[key] = Key.new(key, value)
        end
      end

      # find key
      # @api public
      # @param key [String] registry key
      # @return [Key]
      def find_key(key)
        store[key]
      end

      # find keys
      # @api public
      # @param match_spec [String] string to match against
      # @return [Array]
      def find_keys(match_spec)
        matches = store.keys.select do |key|
          key =~ /#{match_spec}/
        end
        matches.map do |match|
          store[match]
        end
      end

      # find key value
      # @api public
      # @param id [String] registry key
      # @return [Object]
      def find_value(id)
        key = find_key(id)
        key ? key.value : nil
      end

      # find key values
      # @api public
      # @param match [String] string to match against
      # @return [Array]
      def find_values(match)
        keys = find_keys(match)
        keys.map(&:value)
      end

      private

      # store
      # @api private
      # @return [Hash]
      def store
        @store ||= {}
        @store
      end
    end
  end
end
