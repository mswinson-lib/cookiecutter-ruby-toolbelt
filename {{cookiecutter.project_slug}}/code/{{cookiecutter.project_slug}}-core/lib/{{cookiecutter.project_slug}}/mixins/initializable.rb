# rubocop:disable Style/Documentation
require '{{cookiecutter.project_slug}}/mixins/initializable/initializer'

module {{cookiecutter.project_klass}}
  module Mixins
    module Initializable
      # include this module
      # @api public
      # @param base [Module] host class/module
      # @return void
      def self.included(base)
        base.extend(ClassMethods)
      end

      module ClassMethods
        # register an initializer block
        # @api public
        # @param block [Proc] initialization block
        # @return void
        def initializer(&block)
          initializers << Initializer.new(block.to_proc)
        end

        # run initializers
        # @api public
        # @param env [Environment] initialization context
        # @return void
        def run_initializers(env = nil)
          initializers.each do |initializer|
            initializer.run(env)
          end
        end

        # list of initializers
        # @api private
        # @return [Array]
        def initializers
          @initializers ||= []
          @initializers
        end
      end
    end
  end
end
