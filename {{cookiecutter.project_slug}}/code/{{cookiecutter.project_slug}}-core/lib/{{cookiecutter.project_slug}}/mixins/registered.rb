# rubocop:disable Style/Documentation
module {{cookiecutter.project_klass}}
  module Mixins
    module Registered
      # include this module
      # @api public
      # @param base [Class] host class
      # @return void
      def self.included(base)
        base.extend(ClassMethods)
        base.register_class
      end

      # plugin registry
      # @api public
      # @return [Registry]
      def self.registry
        @registry ||= Registry
        @registry
      end

      # provider_id - instance helper
      # @api public
      # @return [String]
      def provider_id
        self.class.provider_id
      end

      # fq_provider_id - instance helper
      # @api public
      # @return [String]
      def fq_provider_id
        self.class.fq_provider_id
      end

      module ClassMethods
        # register subclass on inheritance
        # @api public
        # @param klass [Class] subclass
        # @return void
        def inherited(klass)
          klass.register_class
        end

        # register class
        # @api public
        # @return void
        def register_class
          registry.register(key, self)
        end

        # set/get provider id
        # @api public
        # @param id [String] provider id
        # @return [Symbol]
        def provider_id(id = nil)
          @provider_id ||= local_classname.downcase.to_sym if name
          @provider_id = id.downcase.to_sym if id
          @provider_id
        end

        # full qualified provider id
        # @api public
        # @return [String]
        def fq_provider_id
          if superclass.respond_to?(:provider_id)
            "#{superclass.fq_provider_id}.#{provider_id}"
          else
            "provider.#{provider_id}"
          end
        end

        # registry key
        # @api public
        # @return [String]
        def key
          "#{fq_provider_id}.class"
        end

        # plugin registry
        # @api private
        # @return [Registry]
        def registry
          Registered.registry
        end

        private

        # get local classname
        # @api private
        # @return [String]
        def local_classname
          name.split(/::/).last
        end
      end
    end
  end
end
