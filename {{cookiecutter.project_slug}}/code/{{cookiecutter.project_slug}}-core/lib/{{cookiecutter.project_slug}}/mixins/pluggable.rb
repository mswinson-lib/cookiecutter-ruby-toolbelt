# rubocop:disable Style/Documentation
module {{cookiecutter.project_klass}}
  module Mixins
    module Pluggable
      # include this module
      # @api public
      # @param base [Class] host class
      # @return void
      def self.included(base)
        base.include(Registered)
        base.extend(ClassMethods)
      end

      module ClassMethods
        # find provider classes
        # @api public
        # @return [Array]
        def find_providers
          find_key = "#{fq_provider_id}.(.+)(\.(.+))?.class"

          providers = registry.find_values(find_key)
          providers.compact
        end

        # find provider
        # @api public
        # @param id [String] provider id
        # @return [Array]
        def find_provider(id)
          subclass = self

          subclass_ids = id.to_s.split('.')
          while (subclass_id = subclass_ids.shift)
            subclass = subclass.find_subclass(subclass_id)
          end
          subclass
        end

        # find subclass
        # @api public
        # @param id [String] subclass id
        # @return [Class]
        def find_subclass(id)
          return self if id.to_sym == provider_id

          key = "#{fq_provider_id}.#{id}.class"
          registry.find_value(key)
        end

        # create by id
        # @api public
        # @param id [String] type id
        # @param args [Array] arguments
        # @return [Object]
        def create(id, *args)
          klass = find_provider(id)

          return unless klass

          klass.new(*args)
        end
      end
    end
  end
end
