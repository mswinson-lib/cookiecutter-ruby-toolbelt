module {{cookiecutter.project_klass}}
  module Mixins
    module Initializable
      # thin wrapper around an initialization block
      class Initializer
        # has this been run
        # @api public
        # @return [TrueClass]
        # @return [FalseClass]
        attr_reader :initialized

        # run initializer
        # @api public
        # @param env [Environment] initialization context
        # @return void
        def run(env = nil)
          return if initialized

          @initialized = true
          @callback.call(env)
        end

        private

        # initialize class with initialization block
        # @api private
        # @param callback [Proc] callback
        def initialize callback
          @initialized = false
          @callback = callback
        end
      end
    end
  end
end
