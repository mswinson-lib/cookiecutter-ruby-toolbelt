# rubocop:disable Style/Documentation
module {{cookiecutter.project_klass}}
  module Mixins
    module Configurable
      # include this module into host
      # @api public
      # @param base [Module] module to include
      # @return void
      def self.included(base)
        base.extend(ClassMethods)
      end

      module ClassMethods
        # register configuration block
        # @api public
        # @param block [Proc] configuration block
        # @return void
        def configure
          yield(config) if block_given?
        end

        # configuration
        # @api private
        # @return void
        def config
          @config ||= OpenStruct.new
          @config
        end
      end
    end
  end
end
