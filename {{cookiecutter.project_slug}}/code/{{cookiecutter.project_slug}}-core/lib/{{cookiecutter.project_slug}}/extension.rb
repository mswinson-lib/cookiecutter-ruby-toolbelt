module {{cookiecutter.project_klass}}
  # extension class
  class Extension
    include Mixins::Pluggable
    include Mixins::Initializable
    include Mixins::Configurable

    class << self
      # describe this extension
      # @api public
      # @param description [String] description
      # @return [String]
      def description(description = nil)
        description ||= ''
        @description ||= description
        @description
      end

      # extension environment
      # @api public
      # @return [Environment]
      def env
        return @env if @env

        @env = Environment.new
        @env.config = config
        @env
      end
    end
  end
end
