# rubocop:disable Style/DocumentationMethod
module {{cookiecutter.project_klass}}
  module CLI
    # cli shell
    # @api public
    class Shell < Command
      desc 'version', 'display current version'
      def version
        $stdout.puts({{cookiecutter.project_klass}}::VERSION)
      end

      desc 'console', 'run console'
      def console
        require 'irb'
        ARGV.clear
        IRB.start
      end
    end
  end
end
