# rubocop:disable Metrics/MethodLength, Metrics/LineLength
module {{cookiecutter.project_klass}}
  module CLI
    # shell runner
    # @api public
    class Runner
      # initializes runner
      # @api private
      # @param argv [Array] arguments array
      # @param env [Object] context object
      # @param stdin [IO] input stream
      # @param stdout [IO] output stream
      # @param stderr [IO] error stream
      # @oaram kernel [Kernel] kernel class
      def initialize argv, env = nil, stdin = STDIN, stdout = STDOUT, stderr = STDERR, kernel = Kernel
        @argv = argv
        @env = env
        @stdin = stdin
        @stdout = stdout
        @stderr = stderr
        @kernel = kernel
      end

      # executes runner
      # @api public
      def execute!
        exit_code = begin
          $stderr = @stderr
          $stdin = @stdin
          $stdout = @stdout
          Shell.start(@argv, env: @env)
          0
        rescue StandardError => e
          b = e.backtrace
          @stderr.puts("#{b.shift}: #{e.message} (#{e.class})")
          @stderr.puts(b.map { |s| "\tfrom #{s}" }.join("\n"))
          1
        rescue SystemExit => e
          e.status
        ensure
          $stderr = STDERR
          $stdin = STDIN
          $stdout = STDOUT
        end

        @kernel.exit(exit_code)
      end
    end
  end
end
