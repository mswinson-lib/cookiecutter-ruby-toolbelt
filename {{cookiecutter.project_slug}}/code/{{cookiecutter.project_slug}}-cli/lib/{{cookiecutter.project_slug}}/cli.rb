module {{cookiecutter.project_klass}}
  module Extensions
    # cli extension
    # @api public
    class CLIExtension < Extension
      include Commands

      description 'command line support'

      configure do |_config|
        require '{{cookiecutter.project_slug}}/cli/shell'
        require '{{cookiecutter.project_slug}}/cli/runner'
      end

      initializer do |_env|
        ShellCommand.find_providers.each do |command|
          name = command.command_name
          description = command.command_description

          CLI::Shell.register(command, name, name, description)
        end
      end

      initializer do |env|
        CLI::Runner.new(ARGV, env).execute!
      end
    end
  end
end
