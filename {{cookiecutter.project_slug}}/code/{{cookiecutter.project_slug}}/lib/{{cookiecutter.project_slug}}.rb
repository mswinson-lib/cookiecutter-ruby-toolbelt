require '{{cookiecutter.project_slug}}/version'
require 'bundler/setup'
Bundler.setup(:defaults)
require '{{cookiecutter.project_slug}}/core'
require '{{cookiecutter.project_slug}}/commands'
require '{{cookiecutter.project_slug}}/cli'
require '{{cookiecutter.project_slug}}/app'
