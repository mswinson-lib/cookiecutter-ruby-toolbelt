module {{cookiecutter.project_klass}}
  class App < Extension
    # aggregates configs from all extensions
    class AppConfig
      # find config by key
      # @api public
      # @param key [String] extension id
      # @return [Environment]
      def [](key)
        extension = Extension.find_provider(key)
        extension.config
      end

      # forward properties to extension config
      # @api public
      # @param m [String] method name
      # @param args [Array] method arguments
      # @param block [Proc] callback
      def method_missing m, *_args, &_block
        return if m[-1] == '='

        extension_name = m
        extension = Extension.find_provider(extension_name)
        extension.config
      end
    end
  end
end
