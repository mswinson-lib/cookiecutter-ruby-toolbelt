require '{{cookiecutter.project_slug}}/app/config'

module {{cookiecutter.project_klass}}
  # top level application module
  # @api public
  class App < Extension
    # app config
    # @api public
    # @return [AppConfig]
    def self.config
      @config ||= AppConfig.new
      @config
    end

    configure do |_config|
      require '{{cookiecutter.project_slug}}/commands'
      require '{{cookiecutter.project_slug}}/cli'
    end

    initializer do |_env|
      Extension.find_providers.each do |extension|
        next if extension == self

        extension.run_initializers(env)
      end
    end
  end
end
