require 'simplecov'
require 'simplecov-json'
SimpleCov.formatters = [
  SimpleCov::Formatter::HTMLFormatter,
  SimpleCov::Formatter::JSONFormatter
]
SimpleCov.start do
  add_filter '/spec'
end

support_files = Dir.glob(::File.expand_path('../support/*.rb', __FILE__))
support_files.each { |f| require_relative f }
