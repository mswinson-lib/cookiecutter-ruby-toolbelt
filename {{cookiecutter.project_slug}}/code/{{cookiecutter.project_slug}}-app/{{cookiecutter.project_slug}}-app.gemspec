lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require '../{{cookiecutter.project_slug}}/lib/{{cookiecutter.project_slug}}/version'

Gem::Specification.new do |spec|
  spec.name          = '{{cookiecutter.project_slug}}-app'
  spec.version       = {{cookiecutter.project_klass}}::VERSION
  spec.authors       = ['{{cookiecutter.author_name}}']
  spec.email         = ['{{cookiecutter.author_email}}']
  spec.summary       = '{{cookiecutter.project_summary}}'
  spec.homepage      = '{{cookiecutter.project_home}}'
  spec.license       = 'MIT'

  spec.files         = [
    Dir.glob('{bin,lib}/**/*'),
    %w[LICENSE.txt README.md CHANGELOG.md]
  ].flatten

  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_runtime_dependency '{{cookiecutter.project_slug}}-core', {{cookiecutter.project_klass}}::VERSION
  spec.add_runtime_dependency '{{cookiecutter.project_slug}}-commands', {{cookiecutter.project_klass}}::VERSION
  spec.add_runtime_dependency '{{cookiecutter.project_slug}}-cli', {{cookiecutter.project_klass}}::VERSION
end
