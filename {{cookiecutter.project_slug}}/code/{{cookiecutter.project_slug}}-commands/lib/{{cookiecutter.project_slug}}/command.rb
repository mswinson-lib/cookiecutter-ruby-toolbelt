module {{cookiecutter.project_klass}}
  # base command
  # @api public
  class Command < Thor
    include Mixins::Pluggable

    attr_reader :env

    # get/set command name
    # @api public
    # @return [String]
    def self.command_name(name = nil)
      @name ||= provider_id
      @name = name if name
      @name
    end

    # get/set command description
    # @api public
    # @return [String]
    def self.command_description(description = nil)
      @description ||= ''
      @description = description if description
      @description
    end

    private

    # initialize command
    # @api private
    def initialize(args = [], local_options = {}, config = {})
      env = config[:env]
      @env = env

      super
    end
  end
end
