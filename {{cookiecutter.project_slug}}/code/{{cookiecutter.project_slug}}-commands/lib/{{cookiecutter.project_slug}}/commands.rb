module {{cookiecutter.project_klass}}
  module Extensions
    # add command extensions
    # @api public
    class CommandsExtension < Extension
      description 'command support'

      configure do |_env|
        require 'thor'
        require '{{cookiecutter.project_slug}}/command'
        require '{{cookiecutter.project_slug}}/action'
        require '{{cookiecutter.project_slug}}/commands/shell_command'
      end
    end
  end
end
