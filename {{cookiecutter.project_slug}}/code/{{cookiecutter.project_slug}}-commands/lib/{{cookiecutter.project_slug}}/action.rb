# rubocop:disable Style/DocumentationMethod
module {{cookiecutter.project_klass}}
  # base action
  # @api public
  class Action < Command
    desc 'execute', 'execute action'
    def execute
    end

    default_task :execute
  end
end
